# Retro's Ambience

Ambient noises bound to custom crafted note-blocks.

![vanilla](https://img.shields.io/badge/DESIGNED%20FOR-VANILLA+%20SERVERS-GREEN?style=for-the-badge) ![1.12.x+](https://img.shields.io/badge/1.12+-SUPPORTED-green?style=for-the-badge) ![1.15.x](https://img.shields.io/badge/1.15+-FULL%20SUPPORT-green?style=for-the-badge)

![crafting](https://i.imgur.com/F13s51x.png)

Retro's Ambience brings ambient sounds, and any other sound in Minecraft to note blocks! It comes loaded with
lots of custom crafting recipes to get started, but in only three lines you can add your own. The note blocks can auto-play if enabled in the config, otherwise they act just like normal note-blocks, redstone, pitch and all.

## Build

To build clone the repo and run `mvn`. The compile plugin will be in the `target` directory.

## Ambient Blocks

Ambient blocks are simply note blocks with some custom data added to them. They work just like any other note block but are crafted in a special way.

## Crafting

Each ambient block has it's own crafting recipe, namely, they each have a unique item that is required to be combined with the note block to craft the custom sound. New recipes can be added to the `config.yml` to create your own ambient blocks for your build or server.

### Included Blocks

- **Note Block** + **Beehive** = Beehive Ambience
- **Note Block** + **Ink Sack** = Aquatic Life
- **Note Block** + **Nautilus Shell** = Deep Ocean
- **Note Block** + **Heart of the Sea** = Deeper Ocean
- **Note Block** + **Totem of Undying** = Vex Chuckles
- **Note Block** + **Conduit** = Deep Breaths
- **Note Block** + **Anvid** = Blacksmith's Shop
- **Note Block** + **Bell** = Bell's Ring
- **Note Block** + **Torch** = Gone Caving
- **Note Block** + **Nether Star** = Three-headed Whispers
- **Note Block** + **Dragon Head** = Dragon's Growl
- **Note Block** + **Ghast Tear** = Ghast Ambience
- **Note Block** + **Wither Skeleton Skull** = Wither Skeleton Ambient

## Creating a Custom Recipe

Follow the style in the config for each block outlined below:

```yml
cave:
    sound: AMBIENT_CAVE
    item: TORCH
    name: Gone Caving
```

1. Each block must have a unique section, this should be lowercase and not contain special characters.
2. Next pick out your sound from [here](https://hub.spigotmc.org/javadocs/spigot/org/bukkit/Sound.html).
3. Now chose the item that should be combined with a note block to craft it, again this should be unique. You can find a list of item names [here](https://hub.spigotmc.org/javadocs/bukkit/org/bukkit/Material.html).
4. Lastly the name of the block can be anything you'd like.

## Configuration

### Auto-Play

If disabled, you will only be able to play sounds with redstone

```yml
autoplay: true
```

### Require Power

If disabled, blocks will auto-play without being powered

```yml
require-power: true
```

### Frequency

Minimum frequency for ambient sounds (Below 200 not suggested.)

```yml
frequency: 1200
```

### Likelyhood

To make ambient noises seem less monotinuous they will not play every time the auto-play loop hits.
Each loop there will be an x percent chance the sound plays as configured by `likelyhood`.

```yml
likelyhood: 0.50
```

### Disable Particles

If you'd like to hide ambient blocks in your build it may be more appealing to hide the note particle. You can do this by disabling particle effects.

```yml
disable-particles: false
```

## Language

All language can be configured in `config.yml` to fit your language, or to just change things up.

```yml
language:
  description: Plays ambient sounds
```
