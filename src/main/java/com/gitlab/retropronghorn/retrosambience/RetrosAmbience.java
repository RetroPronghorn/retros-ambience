package com.gitlab.retropronghorn.retrosambience;

import java.util.HashMap;
import java.util.Random;
import java.util.Set;

import com.gitlab.retropronghorn.retrosambience.events.BlockBreakEvents;
import com.gitlab.retropronghorn.retrosambience.events.BlockPlaceEvents;
import com.gitlab.retropronghorn.retrosambience.events.NotePlayEvents;
import com.gitlab.retropronghorn.retrosambience.helpers.StorageHelper;
import com.gitlab.retropronghorn.retrosambience.recipes.Recipe;
import com.gitlab.retropronghorn.retrosutils.location.ULocation;
import com.gitlab.retropronghorn.retrosutils.server.ULogger;
import com.gitlab.retropronghorn.retrosutils.sound.USound;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.NamespacedKey;
import org.bukkit.Particle;
import org.bukkit.Sound;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.ShapedRecipe;
import org.bukkit.inventory.meta.KnowledgeBookMeta;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.util.Vector;


/**
 * Retro's Ambience
 * RetroPronghorn <retropronghorn@gmail.com>
 * https://gitlab.com/RetroPronghorn/retros-ambience
 */
public final class RetrosAmbience extends JavaPlugin {
    public static final String NAME = "RetrosAmbience";
    public static final Long MIN_FREQ = 60L;
    public Recipe recipe;
    private int ambientLoop;
    private StorageHelper storage;
    private final Random random = new Random();
    public final HashMap<Location, String> ambientBlocks = new HashMap<>();

    @Override
    public void onEnable() {
        // Copy configs
        saveDefaultConfig();
        // Bind instance
        recipe = new Recipe(this);
        // Register listeners
        getServer().getPluginManager().registerEvents(new BlockBreakEvents(this), this);
        getServer().getPluginManager().registerEvents(new BlockPlaceEvents(this), this);
        getServer().getPluginManager().registerEvents(new NotePlayEvents(this), this);
        // Load custom crafting recipes
        loadRecipes();
        // Start tick loop
        if (getConfig().getBoolean("autoplay")) {
            ambientLoop = Bukkit.getServer()
                .getScheduler()
                .scheduleSyncRepeatingTask(this, new Runnable() {
                    public void run() {
                        loop();
                    }
                }, 0, getConfig().getLong("frequency"));

            // Logging
            ULogger.info(NAME, "Auto-play enabled. Started auto-play loop.");
            if (getConfig().getLong("frequency") < MIN_FREQ)
                ULogger.warning(NAME, "Auto-play frequency below reccomended, this could be annoying.");
        }
        // Load storage
        storage = new StorageHelper(this);
        storage.keys().forEach(locString -> {
            Location location = ULocation.fromString(locString.replace("*", "."));
            ambientBlocks.put(location, storage.get(locString));
        });
        ULogger.info(NAME, "Loaded " + ambientBlocks.size() + " ambient blocks.");
    }

    @Override
    public void onDisable() {
        ULogger.info(NAME, "Unloading plugin, saving blocks & cleaning up auto-play.");
        // Cancel tick loop
        if (getConfig().getBoolean("autoplay"))
            Bukkit.getServer().getScheduler().cancelTask(ambientLoop);
        // Store ambient blocks
        ambientBlocks.forEach((loc, key) -> {
            storage.set(
                ULocation.toLocationString(loc).replace(".", "*"),
                key
            );
        });
        storage.save();
        ULogger.info(NAME, "Saved " + ambientBlocks.size() + " ambient blocks.");
    }

    /**
     * Loads custom recipes into the server from config.yml's ambient-blocks
     */
    private void loadRecipes() {
        final ConfigurationSection recipes = getConfig().getConfigurationSection("ambient-blocks");
        recipes.getKeys(false).forEach(key -> {
            final ShapedRecipe r = recipe
                    .parseShapedRecipe(getConfig().getConfigurationSection("ambient-blocks." + key), key);
            Bukkit.addRecipe(r);
        });
        recipe.loadKnowledgeBook();
        // Logging
        ULogger.info(NAME, "Loaded " + recipes.getKeys(false).size() + " crafting recipes.");
    }

    /**
     * Looping logic executed by scheduler every x ticks
     */
    private void loop() {
        ambientBlocks.keySet().forEach(key -> {
            Double chance = random.nextDouble();
            if (!getConfig().getBoolean("require-power") ||
                key.getBlock().isBlockPowered()) {
                if (chance <= getConfig().getDouble("likelyhood")) {
                    Sound sound = USound.soundOrFallback(
                        getConfig().getString("ambient-blocks." + ambientBlocks.get(key) + ".sound")
                    );
                    Location offset = key.clone().add(new Vector(0.5, 1, 0.5));
                    key.getWorld().playSound(key, sound, 1F, 1F);
                    // Check to see if we're disabling particles
                    if (!getConfig().getBoolean("disable-particles"))
                        key.getWorld().spawnParticle(Particle.NOTE, offset, 1);
                }
            }
        });
    }

    /**
     * Add a new note block to the loop
     * @param location location of the note block
     */
    public void addBlock(final Location location, final String key) {
        ambientBlocks.put(location, key);
    }

    /**
     * Remove a note block from the loop
     * @param location location of the note block
     */
    public void removeBlock(final Location location) {
        ambientBlocks.remove(location);
    }

    /**
     * Check if a given item is ambientBlock
     * @param item item to check
     * @return returns wether or not the given block is an ambient block
     */
    public Boolean isAbmientBlock(ItemStack item) {
        if (item.getType() == Material.NOTE_BLOCK) {
            Set<String> ambientBlockKeys = getConfig()
                .getConfigurationSection("ambient-blocks")
                .getKeys(false);
            String itemKey = item.getItemMeta().getLore().get(1);
            return ambientBlockKeys.contains(itemKey);
        } else {
            return false;
        }
    }

    /**
     * Check if a location contains an ambient block
     * @param location location to get block from
     * @return returns wether or not a location contains an ambient block
     */
    public Boolean isLoadedAmbientBlock(Location location) {
        return ambientBlocks.containsKey(location);
    }
}
