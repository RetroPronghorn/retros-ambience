package com.gitlab.retropronghorn.retrosambience.recipes;

import java.util.Arrays;

import com.gitlab.retropronghorn.retrosambience.RetrosAmbience;
import com.gitlab.retropronghorn.retrosutils.item.UItem;
import com.gitlab.retropronghorn.retrosutils.material.UMaterial;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.NamespacedKey;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.ShapedRecipe;
import org.bukkit.inventory.meta.KnowledgeBookMeta;

public class Recipe {
    RetrosAmbience instance;

    /**
     * Construct a new Recipe helper
     * @param instance reference to the main plugin class
     */
    public Recipe(RetrosAmbience instance) {
        // Bind main plugin instance
        this.instance = instance;
    }

    public void loadKnowledgeBook() {
        // Setup the result
        NamespacedKey key = new NamespacedKey(instance, "nbkb");
        ShapedRecipe recipe = new ShapedRecipe(key, getKnowledgeBook());
        // Each item will take one custom ingredient
        // as well as a note block to craft
        recipe.shape("B", "N");
        recipe.setIngredient('B', Material.BOOK);
        recipe.setIngredient('N', Material.NOTE_BLOCK);
        // Add the recipe
        Bukkit.addRecipe(recipe);

    }

    public ItemStack getKnowledgeBook() {
        ItemStack item = new ItemStack(Material.KNOWLEDGE_BOOK);
        KnowledgeBookMeta knowledgeBookMeta = (KnowledgeBookMeta) item.getItemMeta();
        knowledgeBookMeta.setDisplayName("Ambient Blocks");

        final ConfigurationSection recipes = instance.getConfig().getConfigurationSection("ambient-blocks");
        recipes.getKeys(false).forEach(key -> {
            knowledgeBookMeta.addRecipe(
                new NamespacedKey(
                    instance,
                    key
                )
            );
        });

        item.setItemMeta(knowledgeBookMeta);
        return item;
    }

    /**
     * Build result itemstack with name and lore data.
     * @param ambientBlock configuration section pointing to the ambient block
     * @param identifier identifying selector for this block
     * @return returns the custom itemstack representing the result
     */
    public ItemStack buildResult(ConfigurationSection ambientBlock, String identifier) {
        return new ItemStack(
            // Create custom item using RetrosUtils
            UItem.customItemStack(
                Material.NOTE_BLOCK,
                "§d§l" + ambientBlock.getString("name"),
                Arrays.asList(
                    instance.getConfig().getString("language.description"),
                    identifier,
                    ambientBlock.getString("sound")
                )
            )
        );
    }

    /**
     * Build result itemstack with name and lore data.
     * @param ambientBlock configuration section pointing to the ambient block
     * @param identifier identifying selector for this block
     * @return returns the custom itemstack representing the result
     */
    public void dropResult(String identifier, Location location) {
        ConfigurationSection ambientBlock = instance.getConfig()
            .getConfigurationSection("ambient-blocks." + identifier);
        ItemStack result = new ItemStack(
            // Create custom item using RetrosUtils
            UItem.customItemStack(
                Material.NOTE_BLOCK,
                "§d§l" + ambientBlock.getString("name"),
                Arrays.asList(
                    instance.getConfig().getString("language.description"),
                    identifier,
                    ambientBlock.getString("sound")
                )
            )
        );
        location.getWorld().dropItem(location, result);
    }

    /**
     * Parse a shaped recipe from the configuration selection provided
     * @param ambientBlock configuration section pointing to the ambient block to parse
     * @param identifier identifying selector for this block, used for future reference
     */
    public ShapedRecipe parseShapedRecipe(ConfigurationSection ambientBlock, String identifier) {
        // Setup the result
        ItemStack result = buildResult(ambientBlock, identifier);
        NamespacedKey key = new NamespacedKey(instance, identifier);
        ShapedRecipe recipe = new ShapedRecipe(key, result);
        // Each item will take one custom ingredient
        // as well as a note block to craft
        recipe.shape("I", "N");
        recipe.setIngredient(
            'I',
            UMaterial.materialOrFallback(
                ambientBlock.getString("item")
            )
        );
        recipe.setIngredient('N', Material.NOTE_BLOCK);
        // Add the recipe
        return recipe;
    }
}