package com.gitlab.retropronghorn.retrosambience.events;

import com.gitlab.retropronghorn.retrosambience.RetrosAmbience;

import org.bukkit.Location;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;

public class BlockPlaceEvents implements Listener {
    private final RetrosAmbience instance;

    /**
     * Construct a new block place listener
     * @param instance reference to the main class
     */
    public BlockPlaceEvents(RetrosAmbience instance) {
        this.instance = instance;
    }

    @EventHandler
    public void blockPlaceEvent(BlockPlaceEvent event) {
        PlayerInventory inventory = event.getPlayer().getInventory();
        // Check if the block is a Ambient Emitter
        Location location = event.getBlock().getLocation();
        ItemStack mainHand = inventory.getItemInMainHand();
        ItemStack offHand = inventory.getItemInOffHand();
        if (instance.isAbmientBlock(mainHand)) {
            // Add a new block to the tick loop
            instance.addBlock(location, mainHand.getItemMeta().getLore().get(1));
        } else if (instance.isAbmientBlock(offHand)) {
            // Add a new block to the tick loop
            instance.addBlock(location, offHand.getItemMeta().getLore().get(1));
        }
    }
}