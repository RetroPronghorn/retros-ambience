package com.gitlab.retropronghorn.retrosambience.events;

import com.gitlab.retropronghorn.retrosambience.RetrosAmbience;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;

public class BlockBreakEvents implements Listener {
    private RetrosAmbience instance;

    /**
     * Constructs a new BlockBreakEvent handler
     * @param instance reference to main plugin
     */
    public BlockBreakEvents(RetrosAmbience instance) {
        this.instance = instance;
    }

    @EventHandler
    public void blockBreakEvent(BlockBreakEvent event) {
        Location location = event.getBlock().getLocation();
        if (instance.isLoadedAmbientBlock(location)) {
            // Cancel normal break to avoid duplication
            event.setCancelled(true);
            event.getBlock().setType(Material.AIR);
            // Create and drop a new ambient block
            String identifier = instance.ambientBlocks.get(location);
            instance.recipe.dropResult(identifier, location);
        }
    }

}