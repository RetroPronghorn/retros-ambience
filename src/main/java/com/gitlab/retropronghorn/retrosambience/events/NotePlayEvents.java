package com.gitlab.retropronghorn.retrosambience.events;

import java.util.Random;

import com.gitlab.retropronghorn.retrosambience.RetrosAmbience;
import com.gitlab.retropronghorn.retrosambience.helpers.NoteHelper;
import com.gitlab.retropronghorn.retrosutils.particle.UParticle;
import com.gitlab.retropronghorn.retrosutils.sound.USound;

import org.bukkit.Location;
import org.bukkit.Particle;
import org.bukkit.Sound;
import org.bukkit.World;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.NotePlayEvent;
import org.bukkit.util.Vector;

public class NotePlayEvents implements Listener {
    private final RetrosAmbience instance;

    /**
     * Construct a new note play event handler
     * @param instance reference to the main plugin class
     */
    public NotePlayEvents(RetrosAmbience instance) {
        this.instance = instance;
    }

    // Priority needs to be highest here to actually cancel
    // the note event if we need to.
    @EventHandler(priority = EventPriority.HIGHEST)
    public void notePlayEvent(NotePlayEvent event) {
        Location location = event.getBlock().getLocation();
        if (instance.isLoadedAmbientBlock(location)) {
            // Only cancel the event if the block is powered
            // This way the player can adjust the octave
            if(event.getBlock().isBlockPowered())
                event.setCancelled(true);

            World world = event.getBlock().getLocation().getWorld();
            String key = instance.ambientBlocks.get(location);
            // Convert the Tone+Octave to a pitch value
            Double pitch = NoteHelper.pitch.get(
                event.getNote().getTone().toString() + ((Integer) event.getNote().getOctave()).toString()
            );
            // Play the custom sound
            Sound sound = USound.soundOrFallback(
                instance.getConfig().getString("ambient-blocks."+key+".sound")
            );
            Location offset = event.getBlock().getLocation().clone().add(new Vector(0, 1, 0));
            if (!instance.getConfig().getBoolean("disable-particles")) {
                UParticle.createCloud(
                    offset,
                    new Random(),
                    (Particle) UParticle.particleOrFallback("NOTE"),
                    3
                );
            }
            world.playSound(location, sound, 1F, pitch.longValue());
        }
    }
}