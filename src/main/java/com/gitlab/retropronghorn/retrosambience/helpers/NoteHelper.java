package com.gitlab.retropronghorn.retrosambience.helpers;

import java.util.HashMap;

public class NoteHelper {
    // A mapping from Tone+Octave to Double for use in pitch.
    public final static HashMap<String, Double> pitch = new HashMap<String, Double>() {
        private static final long serialVersionUID = -6571058982888440631L;
        {
            put("G0", 0.2);
            put("A0", 0.3);
            put("B0", 0.4);
            put("C0", 0.5);
            put("D0", 0.7);
            put("E0", 0.9);
            put("F0", 1.0);
            put("G1", 1.2);
            put("A1", 1.4);
            put("B1", 1.6);
            put("C1", 1.7);
            put("D1", 1.8);
            put("E1", 1.9);
            put("F2", 2.0);
        }
    };
}