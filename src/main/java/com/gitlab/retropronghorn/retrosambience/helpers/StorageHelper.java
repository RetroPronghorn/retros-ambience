package com.gitlab.retropronghorn.retrosambience.helpers;

import java.io.File;
import java.io.IOException;
import java.util.Set;

import com.gitlab.retropronghorn.retrosambience.RetrosAmbience;

import org.bukkit.configuration.InvalidConfigurationException;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

public class StorageHelper {
    File storageFile;
    protected final FileConfiguration storage;

    /**
     * Construct a Database
     * @param RetrosForge instace to main plugin
     **/
    public StorageHelper(RetrosAmbience inststance) {
        storageFile = new File(inststance.getDataFolder(), "storage.yml");
        if (!storageFile.exists()) {
            storageFile.getParentFile().mkdirs();
            inststance.saveResource("storage.yml", false);
        }
        storage = new YamlConfiguration();
        try {
            storage.load(storageFile);
        } catch (IOException | InvalidConfigurationException e) {
            e.printStackTrace();
        }
    }

    /**
     * Set a new key value store and save
     *
     * @param node path to store at
     * @param value value to store
     */
    public void set(String node, String value) {
        storage.set(node, value);
    }

    /**
     * Get a node value
     *
     * @param location node to get from
     */
    public String get(String node) {
        return storage.getString(node);
    }

    public Set<String> keys() {
        return storage.getKeys(false);
    }

    /**
     * Save the data storage file
     */
    public void save() {
        try {
            storage.save(storageFile);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}